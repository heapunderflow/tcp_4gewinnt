package com.heapunderflow.gewinnt4.model;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class ServerTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void start() {
    }

    @Test
    void stop() {
    }

    @Test
    void send() {
    }

    @Test
    void getWriteBuffer() {
    }

    @Test
    void getReadBuffer() {
    }

    @Test
    void getLocalAddress() {
    }

    @Test
    void getRemoteAddress() {
    }

    @Test
    void writeBufferProperty() {
    }

    @Test
    void readBufferProperty() {
    }

    @Test
    void localAddressProperty() {
    }

    @Test
    void remoteAddressProperty() {
    }

    @Test
    void isRunning() {
    }

    @Test
    void runningProperty() {
    }

    @Test
    void getMessagePrefix() {
    }

    @Test
    void writeToStream() {
    }
}