package com.heapunderflow.gewinnt4.model;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void play() {
    }

    @Test
    void resetField() {
    }

    @Test
    void registerFieldListener() {
    }

    @Test
    void removeFieldListener() {
    }

    @Test
    void bindFieldBidirectional() {
    }

    @Test
    void unbindFieldBidirectional() {
    }

    @Test
    void getWidth() {
    }

    @Test
    void getHeight() {
    }

    @Test
    void getPlayerWon() {
    }

    @Test
    void playerWonProperty() {
    }
}