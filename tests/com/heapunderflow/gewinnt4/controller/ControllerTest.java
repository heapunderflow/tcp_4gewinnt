package com.heapunderflow.gewinnt4.controller;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class ControllerTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void play() {
    }

    @Test
    void start() {
    }

    @Test
    void stop() {
    }

    @Test
    void reset() {
    }

    @Test
    void bindField() {
    }

    @Test
    void unbindField() {
    }

    @Test
    void addServerStateChangedListener() {
    }

    @Test
    void removeServerStateChangedListener() {
    }

    @Test
    void getCurrentPlayer() {
    }

    @Test
    void currentPlayerProperty() {
    }

    @Test
    void getLocalAddress() {
    }

    @Test
    void getLocalAddressProperty() {
    }

    @Test
    void getRemoteAddress() {
    }

    @Test
    void getRemoteAddressProperty() {
    }

    @Test
    void getPlayerWonProperty() {
    }
}