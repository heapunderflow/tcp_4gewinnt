package com.heapunderflow.gewinnt4.controller;

import com.heapunderflow.gewinnt4.exceptions.UnknownTypeException;
import com.heapunderflow.gewinnt4.model.*;
import com.heapunderflow.gewinnt4.model.enums.*;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;

import static com.heapunderflow.gewinnt4.model.Constants.*;

/**
 * Controller between UI and Game and GameServer/GameClient
 */
public class Controller {

    private Game game = new Game(Constants.GAME_COLUMNS, Constants.GAME_ROWS);
    private Remote remote;
    private ConnectorType connectorType;
    private SimpleObjectProperty<Player> currentPlayer = new SimpleObjectProperty<>(Player.LOCAL);

    /**
     * Constructor of the Controller
     * @param type Type of the Controller / Connection (Server/Client)
     * @param address Address to bind to (can be null)
     */
    public Controller(ConnectorType type, String address) {
        connectorType = type;
        switch (connectorType) {
            case SERVER:
                remote = new GameServer();
                currentPlayer.set(Player.REMOTE);
                break;
            case CLIENT:
                remote = new GameClient(address);
                currentPlayer.set(Player.LOCAL);
                break;
            default:
                throw new UnknownTypeException();
        }

        remote.readBufferProperty().addListener((ob, ol, ne) -> {
            try {
                thread_out("HANDLE ! [" + ne + "]");

                String[] data = ne.split("\\|");
                if (data.length != 2) {
                    thread_error("Received invalid input: missing prefix");
                    return;
                }

                if (data[1].equals("R")) {
                    game.resetField();
                    return;
                }

                int pos = Integer.parseInt(data[1]);
                if (pos < 0 || pos >= Constants.GAME_COLUMNS) {
                    thread_error("Range was Invalid: " + pos);
                    return;
                }

                game.play(pos, getCurrentPlayer(), getColor());
                remotePlayed();
            } catch (NumberFormatException e) {
                thread_error("invalid input: " + e);
            }
        });

        remote.writeBufferProperty().addListener((ob, ol, ne) -> thread_out(String.format("Write Updated: [%s->%s]", ol, ne)));
    }

    /**
     * Handles the play action that comes from the gui
     * @param x The column to play at
     */
    public void play(int x) {
        if (currentPlayer.get() != Player.LOCAL) {
            thread_out("Invalid Play");
        } else {
            remote.send(String.valueOf(x));
            game.play(x, getCurrentPlayer(), getColor());
            localPlayed();
        }
    }

    /**
     *  Starts the server/client
     */
    public void start() {
        remote.start();
    }

    /**
     *  Stops the server/client
     */
    public void stop() {
        remote.stop();
    }

    /**
     * Resets the field and sends the reset command to the other player
     */
    public void reset() {
        game.resetField();
        remote.send("R");
    }

    /**
     * Bind a field property
     * @param x X position of the field to bind to
     * @param y Y position of the field to bind to
     * @param other The other property to bind to
     */
    public void bindField(int x, int y, Property<FieldColor> other) {
        game.bindFieldBidirectional(x, y, other);
    }

    /**
     * Unbind a field property
     * @param x X position of the field to unbind from
     * @param y Y position of the field to unbind from
     * @param other Other property to unbind
     */
    public void unbindField(int x, int y, Property<FieldColor> other) {
        game.unbindFieldBidirectional(x, y, other);
    }

    /**
     * Add a listener to the Server state (start/stop)
     * @param lstn the listener
     */
    public void addServerStateChangedListener(ChangeListener<? super Boolean> lstn) {
        this.remote.runningProperty().addListener(lstn);
    }

    /**
     * Remove a listener from the Server state (start/stop)
     * @param lstn the listener
     */
    public void removeServerStateChangedListener(ChangeListener<? super Boolean> lstn) {
        this.remote.runningProperty().removeListener(lstn);
    }

    /**
     * Get the player that is currently in play
     * @return The player
     */
    public Player getCurrentPlayer() {
        return currentPlayer.get();
    }

    /**
     * Get the currentPlayerProperty
     * @return The property
     */
    public SimpleObjectProperty<Player> currentPlayerProperty() {
        return currentPlayer;
    }

    /**
     * Returns the local player address (socket)
     * @return The local address
     */
    public String getLocalAddress() {
        return remote.getLocalAddress();
    }

    /**
     * Get the localAddressProperty
     * @return The property
     */
    public SimpleStringProperty getLocalAddressProperty() {
        return remote.localAddressProperty();
    }

    /**
     * Returns the remote player address (socket)
     * @return The remote address
     */
    public String getRemoteAddress() {
        return remote.getRemoteAddress();
    }

    /**
     * Returns the remoteAddressProperty
     * @return The property
     */
    public SimpleStringProperty getRemoteAddressProperty() {
        return remote.remoteAddressProperty();
    }

    /**
     * get the playerWonProperty
     * @return The property
     */
    public SimpleObjectProperty<Player> getPlayerWonProperty() {
        return game.playerWonProperty();
    }

    // ----------- PRIVATE HELPER FUNCTIONS -----------

    /**
     * Set the player to the local player (aka "The other one played")
     */
    private void remotePlayed() {
        this.currentPlayerProperty().set(Player.LOCAL);
    }

    /**
     * Set the player to the remote player (aka "I played")
     */
    private void localPlayed() {
        this.currentPlayerProperty().set(Player.REMOTE);
    }

    /**
     * Get the current color to display<br>
     * Client is always yellow, Server is always red
     * @return The {@link FieldColor}
     */
    private FieldColor getColor() {
        switch (connectorType) {
            case CLIENT:
                switch (currentPlayer.get()) {
                    case LOCAL:
                        return FieldColor.YELLOW;
                    case REMOTE:
                        return FieldColor.RED;
                    default:
                        throw new UnknownTypeException();
                }
            case SERVER:
                switch (currentPlayer.get()) {
                    case LOCAL:
                        return FieldColor.RED;
                    case REMOTE:
                        return FieldColor.YELLOW;
                    default:
                        throw new UnknownTypeException();
                }
            default:
                throw new UnknownTypeException();
        }
    }
}
