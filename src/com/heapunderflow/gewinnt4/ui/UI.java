package com.heapunderflow.gewinnt4.ui;

import com.heapunderflow.gewinnt4.controller.Controller;
import com.heapunderflow.gewinnt4.model.Constants;
import com.heapunderflow.gewinnt4.model.enums.*;
import javafx.application.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

import java.net.*;

import static com.heapunderflow.gewinnt4.model.Constants.*;

/**
 * The UI
 */
public class UI extends Application {
    private Stage primaryStage;
    private Controller controller;
    private ConnectorType connType;
    private SimpleStringProperty ip = new SimpleStringProperty("zhdgjqbkzwdbkzgCKVWjrruhaokzthtkock4okqtqh4wa");


    /**
     * Program Entry Point
     * @param args Command line Args
     */
    public static void main(String[] args) {
        thread_out("Logger ON");
        launch();
    }

    /**
     * Start the UI
     * @param primaryStage PrimaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setResizable(false);
        serverClientChoice();
        this.primaryStage.show();
    }

    /**
     * Choice between server and client
     */
    private void serverClientChoice() {
        GridPane gp = new GridPane();
        gp.setVgap(16.0);
        gp.setHgap(16.0);
        gp.setPadding(new Insets(16.0));

        Button svr = new Button("Server");
        svr.setPrefWidth(Double.MAX_VALUE);
        svr.setOnAction((act) -> {
            thread_out("Entering ServerMode");
            connType = ConnectorType.SERVER;
            controller = new Controller(connType, null);
            gameScreen();
        });

        Button cli = new Button("Client");
        cli.setPrefWidth(Double.MAX_VALUE);
        cli.setOnAction((act) -> {
            thread_out("Entering client mode");
            connType = ConnectorType.CLIENT;
            try {
                ipConnect();
            } catch (UnknownHostException e) {
                thread_error("Unable to resolve address: " + e);
                e.printStackTrace();
            }
        });

        gp.add(svr, 0, 0, 3, 1);
        gp.add(cli, 0, 1, 3, 1);

        this.primaryStage.setScene(new Scene(gp, 200, 100));
    }

    /**
     * The client needs a ip, it is requested from the user here
     * @throws UnknownHostException If the host-discovery failed
     */
    private void ipConnect() throws UnknownHostException {
        ip.addListener((ob, ol, ne) -> {
            controller = new Controller(connType, ne);
            gameScreen();
        });

        if (connType == ConnectorType.SERVER) {
            ip.set(null);
        } else {
            GridPane gp = new GridPane();
            gp.setHgap(16.0);
            gp.setVgap(16.0);
            gp.setPadding(new Insets(16.0));

            Label ipInfo = new Label("IP: ");
            TextField tf = new TextField(InetAddress.getLocalHost().getHostAddress());
            Button conn = new Button("Connect");
            conn.setPrefWidth(Double.MAX_VALUE);
            conn.setOnAction((act) -> {

                String text = tf.getText().trim();
                if (!text.matches("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}")) {
                    act.consume();
                    return;
                }
                ip.set(text);
            });

            gp.add(ipInfo, 1, 0, 1, 1);
            gp.add(tf, 2, 0, 2, 1);
            gp.add(conn, 0, 1, 4, 1);

            primaryStage.setScene(new Scene(gp, 200, 100));
        }
    }

    /**
     * The actual game screen
     */
    private void gameScreen() {
        // Root Element
        GridPane root = new GridPane();
        root.setGridLinesVisible(false);

        // IP Status label
        Label statusIp = new Label("IP: Unknown");
        statusIp.setPadding(new Insets(10.0));

        // IP Address population listener
        controller.getRemoteAddressProperty().addListener((ob, ol, ne) ->
                Platform.runLater(() -> statusIp.setText("IP: " + (ne == null ? "ERR" : ne))));

        // Player info label
        Label statusCurrentPlayer = new Label(controller.getCurrentPlayer() == Player.REMOTE ? "Other players turn" : "Your turn");
        statusIp.setPadding(new Insets(10.0));
        statusIp.setStyle("-fx-start-margin: 16px; -fx-end-margin: 16px; -fx-padding: 0px 16px 0px 0px;");

        HBox statusSpacer = new HBox();
        HBox.setHgrow(statusSpacer, Priority.ALWAYS);

        HBox statusBar = new HBox();
        statusBar.getChildren().addAll(statusIp, statusSpacer, statusCurrentPlayer);

        // Player changed listener
        controller.currentPlayerProperty().addListener((ob, ol, ne) -> Platform.runLater(() -> {
            switch (ne) {
                case REMOTE:
                    statusCurrentPlayer.setText("Other players turn");
                    break;
                case LOCAL:
                    statusCurrentPlayer.setText("Your turn");
                    break;
                default:
                    statusCurrentPlayer.setText("UnknownPlayerState");
                    break;
            }
        }));

        controller.getPlayerWonProperty().addListener((ob, ol, ne) -> {
            String nextText;
            if (ne == Player.LOCAL) {
                thread_out("You WON !");
                nextText = "You WON !\n";
            } else if (ne == Player.REMOTE) {
                thread_out("You LOST !");
                nextText = "You LOST !\n";
            } else if (ne == Player.BOTH) {
                thread_out("DRAW !");
                nextText = "Draw !\n";
            } else {
                thread_error("Invalid player");
                return;
            }

            Platform.runLater(() -> {
                Dialog<Void> rs = new Dialog<>();
                rs.setHeaderText("");
                rs.setTitle("Restart Game ?");
                rs.setContentText(nextText + "Restart Game ?");
                rs.initModality(Modality.APPLICATION_MODAL);

                ButtonType yesBtn = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
                ButtonType noBtn = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);

                rs.getDialogPane().getButtonTypes().setAll(yesBtn, noBtn);

                rs.getDialogPane().lookupButton(yesBtn).addEventHandler(ActionEvent.ACTION, event -> {
                    thread_out("RESTARTING !");
                    controller.reset();
                });

                rs.getDialogPane().lookupButton(noBtn).addEventHandler(ActionEvent.ACTION, event -> {
                    thread_out("STOPPING !");
                    Platform.exit();
                });

                rs.showAndWait();
            });

        });

        // ----- Setup GamePane
        GridPane gamePane = new GridPane();
        gamePane.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        gamePane.setPadding(new Insets(16.0));
        gamePane.setGridLinesVisible(true);
        gamePane.setStyle("-fx-background-color: black;");

        for (int c = 0; c < Constants.GAME_COLUMNS; c++) {
            CoordinateButton btn = new CoordinateButton(c, 0, "Play " + (c + 1));
            btn.setCenterShape(true);
            final int copy = c;
            btn.setOnAction((act) -> controller.play(copy));
            btn.setMaxWidth(Double.MAX_VALUE);
            //btn.setStyle("-fx-background-color: black; -fx-color: white; -fx-border-color: black; -fx-border-style: solid; -fx-border-width: 2px;");

            if (controller.getCurrentPlayer() == Player.REMOTE) {
                btn.setDisable(true);
            }

            controller.currentPlayerProperty().addListener((ob, ol, ne) -> Platform.runLater(() -> {
                switch (ne) {
                    case REMOTE:
                        btn.setDisable(true);
                        break;
                    case LOCAL:
                        btn.setDisable(false);
                        break;
                    default:
                        break;
                }
            }));
            controller.addServerStateChangedListener((ob, ol, ne) -> Platform.runLater(() -> {
                if (connType == ConnectorType.CLIENT && !ne) {
                    btn.setDisable(true);
                }
            }));

            controller.getPlayerWonProperty().addListener((ob, ol, ne) -> {
                btn.setDisable(true);
            });

            GridPane.setHgrow(btn, Priority.ALWAYS);
            gamePane.add(btn, c, 0);
        }

        for (int x = 0; x < Constants.GAME_COLUMNS; x++) {
            for (int y = 1; y < Constants.GAME_ROWS + 1; y++) {
                CoordinateLabel cl = new CoordinateLabel(x, y - 1);
                controller.bindField(x, y - 1, cl.getStateProperty());
                GridPane.setHgrow(cl, Priority.ALWAYS);
                GridPane.setVgrow(cl, Priority.ALWAYS);
                GridPane.setHalignment(cl, HPos.CENTER);
                cl.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
                gamePane.add(cl, x, y);
            }
        }

        // Put all in root
        root.add(gamePane, 0, 0, 3, 3);
        root.add(statusBar, 0, 4, 3, 1);
        //root.add(statusIp, 0, 4, 2, 1);
        //root.add(statusCurrentPlayer, 2, 4, 1, 1);

        // Add resizing strategy
        ColumnConstraints colConstr = new ColumnConstraints();
        colConstr.setHgrow(Priority.ALWAYS);
        colConstr.setFillWidth(true);

        RowConstraints rowConstr = new RowConstraints();
        rowConstr.setVgrow(Priority.ALWAYS);
        rowConstr.setFillHeight(true);

        root.getColumnConstraints().add(colConstr);
        root.getRowConstraints().add(rowConstr);

        // Create szene
        Scene sc = new Scene(root, 315, 300);

        primaryStage.setScene(sc);

        primaryStage.setTitle("Game");
        primaryStage.setMinWidth(315);
        primaryStage.setMinHeight(300);

        primaryStage.setOnCloseRequest((v) -> controller.stop());
        primaryStage.setResizable(false);
        // start the game
        controller.start();
    }

    /**
     * Stop the gui and exit
     * @throws Exception An error occurred while stopping everything
     */
    @Override
    public void stop() throws Exception {
        super.stop();
        if (controller != null) {
            controller.stop();
        }
    }
}