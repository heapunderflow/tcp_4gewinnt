package com.heapunderflow.gewinnt4.ui;

import com.heapunderflow.gewinnt4.model.enums.FieldColor;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Label;

/**
 * Custom label that saves its coordinates in a grid
 */
public class CoordinateLabel extends Label {
    private int xCord = -1;
    private int yCord = -1;
    /**
     * Style that defines its Size
     */
    private static String STYLE_SIZE =
            "-fx-min-width: 40px; "
                    + "-fx-max-width: 40px; "
                    + "-fx-min-height: 40px; "
                    + "-fx-max-height: 40px;";

    /**
     * Neutral color
     */
    private static String STYLE_COLOR_NEUTRAL =
            "-fx-border-radius: 30px; -fx-border-color: gray; -fx-border-style: solid; -fx-border-width: 2px;";
    /**
     * Red Color
     */
    private static String STYLE_COLOR_RED =
            "-fx-background-color: red; -fx-border-radius: 30px; -fx-border-color: red; -fx-border-style: solid; -fx-borderer-width: 2px; -fx-background-radius: 30px;";
    /**
     * Yellow color
     */
    private static String STYLE_COLOR_YELLOW =
            "-fx-background-color: yellow; -fx-border-radius: 30px; -fx-border-color: yellow; -fx-border-style: solid; -fx-border-width: 2px; -fx-background-radius: 30px;";

    private SimpleObjectProperty<FieldColor> stateProperty = new SimpleObjectProperty<>(FieldColor.BLANK);

    /**
     * Construct a new Label
     * @param x X position
     * @param y Y position
     */
    public CoordinateLabel(int x, int y) {
        super();
        xCord = x;
        yCord = y;
        style();
    }

    /**
     * Construct a new Label
     * @param text Text to display
     * @param x X position
     * @param y Y position
     */
    public CoordinateLabel(String text, int x, int y) {
        super(text);
        xCord = x;
        yCord = y;
        style();
    }

    /**
     * Apply the style based on the current state of StateProperty
     */
    private void style() {
        this.setStyle(STYLE_SIZE + STYLE_COLOR_NEUTRAL);

        this.stateProperty.addListener((ob, ol, ne) -> {
            switch (ne) {
                case RED:
                    this.setStyle(STYLE_SIZE + STYLE_COLOR_RED);
                    break;
                case YELLOW:
                    this.setStyle(STYLE_SIZE + STYLE_COLOR_YELLOW);
                    break;
                case BLANK:
                default:
                    this.setStyle(STYLE_SIZE + STYLE_COLOR_NEUTRAL);
                    break;
            }
        });
    }

    /**
     * @return X Coordinate
     */
    public int getxCord() {
        return xCord;
    }

    /**
     * @return Y Coordinate
     */
    public int getyCord() {
        return yCord;
    }

    /**
     * Get the current state
     * @return Current {@link FieldColor}
     */
    public FieldColor getState() {
        return stateProperty.get();
    }

    /**
     * Get the stateProperty
     * @return The property
     */
    public SimpleObjectProperty<FieldColor> getStateProperty() {
        return stateProperty;
    }
}

