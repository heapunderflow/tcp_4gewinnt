package com.heapunderflow.gewinnt4.ui;

import javafx.scene.control.Button;

/**
 * Custom button that stores its coordinates in a grid
 */
public class CoordinateButton extends Button {
    private int xCord;
    private int yCord;

    /**
     * Construct a new Button
     * @param x X coordinate
     * @param y Y coordinate
     */
    public CoordinateButton(int x, int y) {
        super();
        this.xCord = x;
        this.yCord = y;
    }

    /**
     * Construct a new Button
     * @param x X coordinate
     * @param y Y coordinate
     * @param s Text to Display
     */
    public CoordinateButton(int x, int y, String s) {
        super(s);
        this.xCord = x;
        this.yCord = y;
    }

    /**
     * @return The X coordinate of the Button
     */
    public int getxCord() {
        return xCord;
    }

    /**
     * @return The Y coordinate of the Button
     */
    public int getyCord() {
        return yCord;
    }
}