package com.heapunderflow.gewinnt4.exceptions;

/**
 * Exception for when an unknown enum type was given.
 */
public class UnknownTypeException extends RuntimeException {
    public UnknownTypeException() {
        super();
    }

    public UnknownTypeException(String message) {
        super(message);
    }
}
