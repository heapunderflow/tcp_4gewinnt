package com.heapunderflow.gewinnt4.exceptions;

/**
 * Exception that is thrown when a invalid play was taken.
 */
public class InvalidPlayException extends RuntimeException {
    public InvalidPlayException() {
        super();
    }

    public InvalidPlayException(String msg) {
        super(msg);
    }
}
