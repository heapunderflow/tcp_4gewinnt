package com.heapunderflow.gewinnt4.model;

import com.heapunderflow.gewinnt4.exceptions.InvalidPlayException;
import com.heapunderflow.gewinnt4.model.enums.*;
import javafx.beans.property.*;
import javafx.beans.value.ChangeListener;

import java.security.InvalidParameterException;

public class Game {

    private static final int D_UP = 1;
    private static final int D_DOWN = -1;
    private static final int D_LEFT = -1;
    private static final int D_RIGHT = 1;
    private static final int D_NONE = 0;

    private SimpleObjectProperty<FieldColor>[][] field;
    private SimpleObjectProperty<Player> playerWon = new SimpleObjectProperty<>(null);
    private int xlen;
    private int ylen;

    @SuppressWarnings("unchecked")
    public Game(int width, int height) throws InvalidParameterException {

        if (width < 1 || height < 1) {
            throw new InvalidParameterException();
        }

        this.xlen = width;
        this.ylen = height;
        this.field = new SimpleObjectProperty[xlen][ylen];
        for (int x = 0; x < xlen; x++) {
            for (int y = 0; y < ylen; y++) {
                this.field[x][y] = new SimpleObjectProperty<>(FieldColor.BLANK);
            }
        }
    }

    /**
     * Make a move as a player with a color
     *
     * @param column The column to play at
     * @param player The player to play as
     * @param color  The color to play with
     * @throws InvalidParameterException Throws if the column does not survive the initial sanity check
     * @throws InvalidPlayException      Throws if the player tries to make a invalid move (namely: playing on a full column)
     */
    public void play(int column, Player player, FieldColor color) throws InvalidParameterException, InvalidPlayException {
        if (column < 0 || column > this.xlen) {
            throw new InvalidParameterException();
        }

        // Offset of the height we are currently checking
        int yOffset = ylen - 1;
        if (yOffset < 0) {
            throw new InvalidPlayException("Played on full column: (Column: " + color + ")");
        }
        while (this.field[column][yOffset].get() != FieldColor.BLANK) {
            yOffset--;

            if (yOffset == field[column].length) {
                throw new InvalidPlayException("Column Full");
            }
        }
        field[column][yOffset].set(color);
        hasWonWithPlacement(column, yOffset, player, color);
    }

    /**
     * Clears the field to all Blank
     */
    public void resetField() {
        for (int x = 0; x < getWidth(); ++x) {
            for (int y = 0; y < getHeight(); ++y) {
                this.field[x][y].set(FieldColor.BLANK);
            }
        }
    }

    /**
     * Checks if the player has won or created a draw by playing at the given position
     *
     * @param x      horizontal coordiante of the play position
     * @param y      vertical coordiante of the play position
     * @param player the player to make the move as
     * @param color  the color to play with
     */
    private void hasWonWithPlacement(int x, int y, Player player, FieldColor color) {
        if (1 + fieldsInRow(x + 1, y, Direction.RIGHT, Direction.NONE, color)
                + fieldsInRow(x - 1, y, Direction.LEFT, Direction.NONE, color) >= 4
                || 1 + fieldsInRow(x, y + 1, Direction.NONE, Direction.UP, color)
                + fieldsInRow(x, y - 1, Direction.NONE, Direction.DOWN, color) >= 4
                || 1 + fieldsInRow(x - 1, y + 1, Direction.LEFT, Direction.UP, color)
                + fieldsInRow(x + 1, y - 1, Direction.RIGHT, Direction.DOWN, color) >= 4
                || 1 + fieldsInRow(x + 1, y + 1, Direction.RIGHT, Direction.UP, color)
                + fieldsInRow(x - 1, y - 1, Direction.LEFT, Direction.DOWN, color) >= 4) {
            playerWonProperty().set(player);
        } else {
            if (isGameFieldFilled()) {
                playerWonProperty().set(Player.BOTH);
            }
        }
    }

    /**
     * @param x X position to check
     * @param y Y position to check
     * @param xDirection X Direction to move in (See {@link Direction#LEFT} and {@link Direction#RIGHT})
     * @param yDirection Y Direction to move in (See {@link Direction#UP} and {@link Direction#DOWN})
     * @param color The color to check against
     * @return Amount of fields in a row
     */
    private int fieldsInRow(int x, int y, Direction xDirection, Direction yDirection, FieldColor color) {
        try {
            if (field[x][y].get() == color) {
                return 1 + fieldsInRow(x + resolveDirection(xDirection), y + resolveDirection(yDirection), xDirection, yDirection, color);
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            return 0;
        }
        return 0;
    }

    /**
     * Checks if the gameField was filled with colors or if there is still space
     * @return Is Filled
     */
    private boolean isGameFieldFilled() {
        for (int x = 0; x < getWidth(); ++x) {
            for (int y = 0; y < getHeight(); ++y) {
                if (field[x][y].get() == FieldColor.BLANK) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Resolves a {@link Direction} to an actual number
     * @param d The direction to resolve
     * @return The number
     */
    private int resolveDirection(Direction d) {
        switch (d) {
            case UP:
                return 1;
            case DOWN:
                return -1;
            case LEFT:
                return -1;
            case RIGHT:
                return 1;
            case NONE:
            default:
                return 0;
        }
    }

    /**
     * Register a listener for a the field at the given position
     * @param x X position of the field
     * @param y Y position of the field
     * @param listener The listener to add
     */
    public void registerFieldListener(int x, int y, ChangeListener<FieldColor> listener) {
        field[x][y].addListener(listener);
    }

    /**
     * Unregister a listener for a the field at the given position
     * @param x X position of the field
     * @param y Y position of the field
     * @param listener The listener to remove
     */
    public void removeFieldListener(int x, int y, ChangeListener<FieldColor> listener) {
        field[x][y].removeListener(listener);
    }

    /**
     * Bind a field bidirectional
     * @param x X position of the field
     * @param y Y position of the field
     * @param prop the other Property to bind
     */
    public void bindFieldBidirectional(int x, int y, Property<FieldColor> prop) {
        field[x][y].bindBidirectional(prop);
    }

    /**
     * Unbind a field bidirectional
     * @param x X position of the field
     * @param y Y position of the field
     * @param prop the other Property to unbind
     */
    public void unbindFieldBidirectional(int x, int y, Property<FieldColor> prop) {
        field[x][y].unbindBidirectional(prop);
    }

    /**
     * Get the width of the GamePane
     * @return Width of GamePane
     */
    public int getWidth() {
        return this.xlen;
    }

    /**
     * Get the height of the GamePane
     * @return height of GamePane
     */
    public int getHeight() {
        return this.ylen;
    }

    /**
     * Get if the player has won
     * @return Player has Won
     */
    public Player getPlayerWon() {
        return playerWon.get();
    }

    /**
     * Get the playerWonProperty
     * @return The property
     */
    public SimpleObjectProperty<Player> playerWonProperty() {
        return playerWon;
    }
}
