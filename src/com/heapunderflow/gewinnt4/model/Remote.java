package com.heapunderflow.gewinnt4.model;

import javafx.beans.property.*;

import java.io.*;

/**
 * Game SERVER and CLIENT Interface
 */
@SuppressWarnings("unused")
public abstract class Remote {

    private int messageOffsetCounter = 0;

    /**
     * Outbound "message" buffer
     */
    private SimpleStringProperty writeBuffer = new SimpleStringProperty();
    /**
     * Inbound "message" buffer
     */
    private SimpleStringProperty readBuffer = new SimpleStringProperty();
    /**
     * Local address storage
     *
     * Used for notifying the ui that the local address has changed
     */
    private SimpleStringProperty localAddress = new SimpleStringProperty();
    /**
     * Remote address storage
     *
     * Used for notifying the ui that the remote address has changed
     */
    private SimpleStringProperty remoteAddress = new SimpleStringProperty();
    /**
     * Run switch
     */
    private SimpleBooleanProperty running = new SimpleBooleanProperty();

    /**
     * Start the remote
     */
    public void start() {
        this.running.set(true);
    }

    /**
     * Stop the remote
     */
    public void stop() {
        this.running.set(false);
    }

    /**
     * Send a string to the connected peer
     *
     * @param content The content to send
     */
    public void send(String content) {
        this.writeBuffer.set(this.getMessagePrefix() + content);
    }

    /**
     * Returns the content of the write-buffer
     *
     * @return Content
     */
    public String getWriteBuffer() {
        return this.writeBuffer.get();
    }

    /**
     * Returns the content of the read-buffer
     *
     * @return Content
     */
    public String getReadBuffer() {
        return this.readBuffer.get();
    }

    /**
     * Returns the current local address
     *
     * @return Address or null
     */
    public String getLocalAddress() {
        return this.localAddress.get();
    }

    /**
     * Returns the current remote address
     *
     * @return Address or null
     */
    public String getRemoteAddress() {
        return this.remoteAddress.get();
    }

    /**
     * Get the outbound property
     *
     * @return property
     */
    public SimpleStringProperty writeBufferProperty() {
        return this.writeBuffer;
    }

    /**
     * Get the inbound property
     *
     * @return property
     */
    public SimpleStringProperty readBufferProperty() {
        return this.readBuffer;
    }

    /**
     * Get the local-address property
     *
     * @return property
     */
    public SimpleStringProperty localAddressProperty() {
        return this.localAddress;
    }

    /**
     * Get the remote-address property
     *
     * @return property
     */
    public SimpleStringProperty remoteAddressProperty() {
        return this.remoteAddress;
    }

    /**
     * Used to signal if anything is running or not
     *
     * @return Running
     */
    public boolean isRunning() {
        return running.get();
    }

    /**
     * Get the running property
     *
     * @return property
     */
    public SimpleBooleanProperty runningProperty() {
        return running;
    }

    /**
     * Returns the "prefix" for the message
     * This is primarily used to force a trigger in the event emitters
     * even if the message content itself does not change (see {@link Remote#writeBuffer} and {@link Remote#readBuffer})
     * @return The prefix
     */
    String getMessagePrefix() {
        return "p" + (++this.messageOffsetCounter) + "|";
    }

    public static void writeToStream(String content, OutputStream stream_out) throws IOException {
        // This time using a buffered writer
        BufferedWriter bufOut = new BufferedWriter(new OutputStreamWriter(stream_out));
        // Write Protocol version
        bufOut.write(Constants.PROTOCOL_VERSION);
        bufOut.write('\n');
        // Write the content
        bufOut.write(content);
        bufOut.write('\n');
        // Flush to connection.
        bufOut.flush();

        // We do not close here, as that would close the underlying socket too
        // we do not want that as the client is currently "single-connection"-mode only
    }
}


