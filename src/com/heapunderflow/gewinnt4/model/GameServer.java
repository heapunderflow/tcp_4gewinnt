package com.heapunderflow.gewinnt4.model;

import java.io.*;
import java.net.*;

import static com.heapunderflow.gewinnt4.model.Constants.*;

/**
 * Represents a server that can host a game <br>
 * It follows the DQI15_PPv0.9 Protocol
 */
public class GameServer extends Remote {

    private ServerSocket serverSocket;
    private Socket clientSocket;

    /**
     * Start the server
     */
    @Override
    public void start() {
        super.start();
        Thread ct = new Thread(Constants.THREAD_GROUP, this::run, "GameServer_Thread");
        ct.setDaemon(true);
        ct.start();
        thread_out("Starting GameServer...");
    }

    /**
     * Stop the server
     */
    @Override
    public void stop() {
        super.stop();
        thread_out("Stopping GameServer...");
    }

    /**
     * Internal worker function for the GameClient thread
     */
    private void run() {
        thread_out("Started GameServer.");

        try {
            serverSocket = new ServerSocket(Constants.PROTOCOL_PORT);
            thread_out("SERVER created... Waiting for connections on [" + serverSocket.getLocalPort() + "]");

            clientSocket = serverSocket.accept(); // Only ever accept 1 connection (because im lazy)

            // "Outbound" messages
            //noinspection Duplicates
            writeBufferProperty().addListener((ob, ol, ne) -> {
                if (clientSocket == null || !clientSocket.isConnected()) {
                    thread_error("Client socket closed: unable to write");
                    // Exit the Handler early as we cannot handle in this state
                    return;
                }

                String[] data = ne.split("\\|");
                if (data.length != 2) {
                    thread_error("Received invalid input: missing prefix");
                    return;
                }
                String out = data[1].trim();

                thread_out("WRITE ! [" + out + "] (CPREF: [" + data[0] + "])");

                try {
                    Remote.writeToStream(out, clientSocket.getOutputStream());
                } catch (IOException e) {
                    thread_out("Error: output stream" + e.getLocalizedMessage());
                    e.printStackTrace();
                }
            });

            // this is to "break" reads
            // it enables us to react to changes in
            clientSocket.setSoTimeout(1000);

            // Populate the address fields
            localAddressProperty().set(serverSocket.getLocalSocketAddress().toString());
            remoteAddressProperty().set(clientSocket.getRemoteSocketAddress().toString());

            thread_out("User connected: " + clientSocket.getRemoteSocketAddress());
            BufferedReader bufIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            while (clientSocket.isConnected() && isRunning()) {
                // noinspection Duplicates
                try {
                    String line = bufIn.readLine();
                    if (!line.trim().equals(Constants.PROTOCOL_VERSION)) {
                        // Protocol Mismatch
                        thread_error("version mismatch. Got [" + line + "] Expected [" + Constants.PROTOCOL_VERSION + "]");
                        continue;
                    }

                    line = bufIn.readLine();
                    thread_out("READ ! [" + line + "]");
                    this.readBufferProperty().set(this.getMessagePrefix() + line.trim());
                } catch (NullPointerException e) {
                    throw new IOException("Connection was closed by the remote host");
                } catch (SocketTimeoutException e) {
                    // This throws away any errors for ReadTimedOut as we
                    // expect that to happen
                    // TODO: Maybe find more elegant solution
                    if (!e.getMessage().equals("Read timed out")) {
                        throw e;
                    }
                }
            }

        } catch (SocketException e) {
            if (e.getMessage().equals("Connection reset")) {
                this.runningProperty().set(false);
                return;
            }
            e.printStackTrace();
        } catch (IOException e) {
            thread_error("Fatal ServerSocket error: " + e.getLocalizedMessage());
            e.printStackTrace();
        } finally {
            if (!serverSocket.isClosed()) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    thread_error("[PANIC] Unable to close serversocket: " + e.getLocalizedMessage());
                    e.printStackTrace();
                }
            }
        }

        thread_out("Stopped GameServer.");
    }
}
