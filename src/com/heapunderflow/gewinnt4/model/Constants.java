package com.heapunderflow.gewinnt4.model;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Constants used throughout the program
 */
public class Constants {
    /**
     * The Version of the Protocol used
     */
    public static final String PROTOCOL_VERSION = "0.9";
    /**
     * The port that the server should listen on
     */
    public static final int PROTOCOL_PORT = 22342;
    /**
     * The amount of columns the game should have
     */
    public static final int GAME_COLUMNS = 7;
    /**
     * The amount of rows the game should have
     */
    public static final int GAME_ROWS = 5;

    /**
     * Thread group of both server and client
     */
    public static ThreadGroup THREAD_GROUP = new ThreadGroup("RemoteWorkerThreads");

    /**
     * Write a message to stdout with datetime, threadid and threadname before the message
     * @param message The message to display
     */
    public static void thread_out(String message) {
        Thread ct = Thread.currentThread();
        ZonedDateTime ld = ZonedDateTime.now();
        System.out.println(
            // Date & Time
            // Thread (P)ID
            // Thread Name
            // Message
            String.format("[%-33s|%5d|%-25s] %s", ld.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME), ct.getId(), ct.getName(), message)
        );
    }

    /**
     * Write a message to stderr with datetime, threadid and threadname before the message
     * @param message The message to display
     */
    public static void thread_error(String message) {
        Thread ct = Thread.currentThread();
        ZonedDateTime ld = ZonedDateTime.now();
        System.err.println(
                // Date & Time
                // Thread (P)ID
                // Thread Name
                // Message
                String.format("[%s|%5d|%-25s] %s", ld.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME), ct.getId(), ct.getName(), message)
        );
    }
}