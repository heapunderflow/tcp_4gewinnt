package com.heapunderflow.gewinnt4.model.enums;

/**
 * Type of the {@link com.heapunderflow.gewinnt4.controller.Controller}
 */
public enum ConnectorType {
    /**
     * Controller is a Server (Hosts a game)
     */
    SERVER,
    /**
     * Controller is a Client (Connects to a game)
     */
    CLIENT
}
