package com.heapunderflow.gewinnt4.model.enums;

/**
 * Type of a player
 */
public enum Player {
    LOCAL,
    REMOTE,
    /**
     * This is ONLY used for the Draw-Flag
     */
    BOTH
}
