package com.heapunderflow.gewinnt4.model.enums;

/**
 * Color of a Field on the GamePane
 */
public enum FieldColor {
    RED,
    YELLOW,
    BLANK
}
