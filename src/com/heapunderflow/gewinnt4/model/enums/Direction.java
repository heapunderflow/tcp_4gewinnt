package com.heapunderflow.gewinnt4.model.enums;

/**
 * Possible directions on the GameField
 */
public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    NONE
}
