package com.heapunderflow.gewinnt4.model;

import java.io.*;
import java.net.*;

import static com.heapunderflow.gewinnt4.model.Constants.*;

/**
 * Represents a client that can connect to a host <br>
 * It follows the DQI15_PPv0.9 Protocol
 */
public class GameClient extends Remote {

    private String ip;
    private Socket clientSocket;

    /**
     * Construct a new GameClient
     * @param ip the ip to connect to on {@link GameClient#start()}
     */
    public GameClient(String ip) {
        this.ip = ip;
    }

    /**
     * Start the Client
     */
    @Override
    public void start() {
        super.start();
        Thread ct = new Thread(null, this::run, "GameClient_Thread");
        ct.setDaemon(true);
        ct.start();
        thread_out("Starting GameClient...");
    }

    /**
     * Stop the client
     */
    @Override
    public void stop() {
        super.stop();
        thread_out("Stopping GameClient...");
    }

    /**
     * Internal worker function for the GameClient thread
     */
    private void run() {
        thread_out("Started GameClient.");

        SocketAddress socketAddress = new InetSocketAddress(ip, Constants.PROTOCOL_PORT);
        clientSocket = new Socket();
        try {
            clientSocket.connect(socketAddress, 10000);

            //noinspection Duplicates
            writeBufferProperty().addListener((ob, ol, ne) -> {
                if (clientSocket == null || !clientSocket.isConnected()) {
                    thread_error("Client socket closed: unable to write");
                    // Exit the Handler early as we cannot handle in this state
                    return;
                }

                String[] data = ne.split("\\|");
                if (data.length != 2) {
                    thread_error("Received invalid input: missing prefix");
                    return;
                }
                String out = data[1].trim();

                thread_out("WRITE ! [" + out + "] (CPREF: [" + data[0] + "])");

                try {
                    Remote.writeToStream(out, clientSocket.getOutputStream());
                } catch (IOException e) {
                    thread_out("Error: output stream" + e.getLocalizedMessage());
                    e.printStackTrace();
                }
            });

            remoteAddressProperty().set(clientSocket.getRemoteSocketAddress().toString());
            localAddressProperty().set(clientSocket.getLocalAddress().toString());

            clientSocket.setSoTimeout(1000);

            BufferedReader bufIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            while (isRunning() && clientSocket.isConnected()) {
                // noinspection Duplicates
                try {
                    String line = bufIn.readLine();
                    if (!line.trim().equals(Constants.PROTOCOL_VERSION)) {
                        // Protocol Mismatch
                        thread_error("version mismatch. Got [" + line + "] Expected [" + Constants.PROTOCOL_VERSION + "]");
                        continue;
                    }

                    line = bufIn.readLine();
                    thread_out("READ ! [" + line + "]");
                    this.readBufferProperty().set(this.getMessagePrefix() + line.trim());
                } catch (NullPointerException e) {
                    throw new IOException("Connection was closed by the remote host");
                } catch (SocketTimeoutException e) {
                    // This throws away any errors for ReadTimedOut as we
                    // expect that to happen
                    // TODO: Maybe find more elegant solution
                    if (!e.getMessage().equals("Read timed out")) {
                        throw e;
                    }
                }
            }

        } catch (ConnectException e) {
            thread_error("Unable to connect to socket [Timeout]: " + e.getMessage());
            this.stop();
        } catch (IOException e) {
            thread_error("Error occured: " + e);
            e.printStackTrace();
        } finally {
            this.stop();
        }

        thread_out("Stopped GameClient.");
    }
}
